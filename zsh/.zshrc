# PATH
export PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH

# SOURCE
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/zsh-history-substring-search/zsh-history-substring-search.zsh

# bind UP and DOWN keys
bindkey "${terminfo[kcuu1]}" history-substring-search-up
bindkey "${terminfo[kcud1]}" history-substring-search-down

# bind UP and DOWN arrow keys (compatibility fallback)
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# COLORS
autoload -U colors && colors

# COMPLETION
zstyle ':completion:*' completer _expand _complete _ignored
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}'

autoload -Uz compinit
compinit

# GIT INFO
autoload -Uz vcs_info

# HISTORY
HISTFILE=~/.zsh/zhistory
HISTSIZE=10000
SAVEHIST=10000
# textual history expansion, csh-style, treating the character ‘!’ specially.
setopt BANG_HIST
# causes all terminals to share the same history 'session'
setopt SHARE_HISTORY
# append to history immediately instead of when shell exits
setopt INC_APPEND_HISTORY
# ignore duplicates of previous event
setopt HIST_IGNORE_DUPS
# if duplicate is to be added, remove older instance in history
setopt HIST_IGNORE_ALL_DUPS
# do not add any commands to history that begin with a space
setopt HIST_IGNORE_SPACE
# when saving, older commands that duplicate newer commands are omitted
setopt HIST_SAVE_NO_DUPS
# upon history 'selection', don't execute immediately. Require a carriage return
setopt HIST_VERIFY
# list 10 most used commands
alias history-stat="history 0 | awk '{print \$2}' | sort | uniq -c | sort -n -r | head"

# DIRECTORY
# If the typed command is invalid, but is a directory, cd to that directory
setopt AUTO_CD
# After cd, push the old directory to the directory stack
setopt AUTO_PUSHD
# Don't push multiple copies of the same directory to the stack
setopt PUSHD_IGNORE_DUPS
# Don't print the directory after pushd or popd
setopt PUSHD_SILENT
# pushd without arguments acts like pushd ${HOME}
setopt PUSHD_TO_HOME
# Treat #, ~, and ^ as patterns for filename globbing
setopt EXTENDED_GLOB
# run "up" to "cd .." or "up 6" to "cd ../../../../../.."
function up {
    if [[ "$#" < 1 ]] ; then
        cd ..
    else
        CDSTR=""
        for i in {1..$1} ; do
            CDSTR="../$CDSTR"
        done
        cd $CDSTR
    fi
}

# ALIASES
alias cp='cp -rvi'
alias chmod='chmod --preserve-root -v'
alias chown='chown --preserve-root -v'
alias rm='rm -rfiv'
alias mkdir='mkdir -p'
alias ...='../..'
alias ....='../../..'
alias grep='grep --color=always'
alias ls='ls -lhAG' 
alias shync='rsync -vrptoDzhpP -e ssh'
alias lhync='rsync -vrptoDzhpP'
alias home='cd ~/'
alias dspace='du -ch -d 1'
alias surl='curl -F c=@- https://wat.krup.me/u <<<'

# COLORED MANUAL PAGES
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

# PROMPT
# Add some git info into prompt 
setopt prompt_subst

git_info() {

  # Exit if not inside a Git repository
  ! git rev-parse --is-inside-work-tree > /dev/null 2>&1 && return

  # Git branch/tag, or name-rev if on detached head
  local GIT_LOCATION=${$(git symbolic-ref -q HEAD || git name-rev --name-only --no-undefined --always HEAD)#(refs/heads/|tags/)}

  local AHEAD="%{$fg[red]%}⇡NUM%{$reset_color%}"
  local BEHIND="%{$fg[cyan]%}⇣NUM%{$reset_color%}"
  local MERGING="%{$fg[magenta]%}⚡︎%{$reset_color%}"
  local UNTRACKED="%{$fg[red]%}●%{$reset_color%}"
  local MODIFIED="%{$fg[yellow]%}●%{$reset_color%}"
  local STAGED="%{$fg[green]%}●%{$reset_color%}"

  local -a DIVERGENCES
  local -a FLAGS

  local NUM_AHEAD="$(git log --oneline @{u}.. 2> /dev/null | wc -l | tr -d ' ')"
  if [ "$NUM_AHEAD" -gt 0 ]; then
    DIVERGENCES+=( "${AHEAD//NUM/$NUM_AHEAD}" )
  fi

  local NUM_BEHIND="$(git log --oneline ..@{u} 2> /dev/null | wc -l | tr -d ' ')"
  if [ "$NUM_BEHIND" -gt 0 ]; then
    DIVERGENCES+=( "${BEHIND//NUM/$NUM_BEHIND}" )
  fi

  local GIT_DIR="$(git rev-parse --git-dir 2> /dev/null)"
  if [ -n $GIT_DIR ] && test -r $GIT_DIR/MERGE_HEAD; then
    FLAGS+=( "$MERGING" )
  fi

  if [[ -n $(git ls-files --other --exclude-standard 2> /dev/null) ]]; then
    FLAGS+=( "$UNTRACKED" )
  fi

  if ! git diff --quiet 2> /dev/null; then
    FLAGS+=( "$MODIFIED" )
  fi

  if ! git diff --cached --quiet 2> /dev/null; then
    FLAGS+=( "$STAGED" )
  fi

  local -a GIT_INFO
  GIT_INFO+=( "\033[38;5;15m±" )
  [ -n "$GIT_STATUS" ] && GIT_INFO+=( "$GIT_STATUS" )
  [[ ${#DIVERGENCES[@]} -ne 0 ]] && GIT_INFO+=( "${(j::)DIVERGENCES}" )
  [[ ${#FLAGS[@]} -ne 0 ]] && GIT_INFO+=( "${(j::)FLAGS}" )
  GIT_INFO+=( "\033[38;5;15m$GIT_LOCATION%{$reset_color%}" )
  echo "${(j: :)GIT_INFO}"

}
case $(hostname -s) in
	behemot) 
		PROMPT=$'🏡 %F{cyan}%n%f on %F{red}%m%f in %F{yellow}%2 %2~ $(git_info)\n%F{yellow}» '
		;;
	vostok) 
		PROMPT=$'🔥 %F{red}%n%f on %F{green}%m%f in %F{blue}%2 %2~ $(git_info)\n%F{yellow}» ' 
		;;
	judas) 
		PROMPT=$'🛰️%F{red}%n%f@%F{magenta}%m%f:%F{blue}%2 %2~ $(git_info)\n%F{yellow}» ' 
		;;
esac
# ! source syntax highlighting, must be at the end
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
