" plugin
call plug#begin('~/.vim/autoload/plugins')
Plug 'luochen1990/rainbow'
Plug 'sheerun/vim-polyglot'
Plug 'junegunn/fzf.vim'
Plug 'itchyny/lightline.vim'
Plug 'scrooloose/nerdtree'
call plug#end()

" plugin options
" rainbow
let g:rainbow_active = 1

" fzf
set rtp+=/usr/local/opt/fzf

" general options
syntax on
filetype plugin indent off
set number relativenumber
set nowrap
set nocindent
set nosmartindent
set noautoindent
set tabstop=2 shiftwidth=2 expandtab
set incsearch
set backspace=indent,eol,start
set laststatus=2

" mappings
map <C-n> :NERDTreeToggle<CR>
